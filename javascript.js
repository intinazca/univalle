$(document).ready(function(){                
    $.ajax({
        //Importar archivos como CSV cambiar para que la persona seleccione el archivo correspondiente
        url: 'RespuestasC.csv',
        dataType: "text",
        contentType: "charset-utf-8"
    }).done(grafica);

        function grafica(data) 
        {       
                /* recibe los datos del excel y los muestra en consola*/
                //console.log(data);
                //quitar saltos de linea
                var datos= data.split(/\r?\n|\r/); /* pone los datos del excel en un lista*/
                
                
                console.log(datos); 

                console.log(datos.length);//Tanmaño siempre con un ultimo dato vacio y el primero en este caso no es util
                
                
                var datosPreguntas = [];
                var Preguntas =[];
                var datosPreguntas2 = [];

                //CICLO PARA SACAR PREGUNTAS CON I=0 TRAE LA PREGUNTA  
                for(var i=0; i < datos.length-1 ;i++){
                    
                    var datosSinComas= datos[i].split(';');
                    var SinMarca = datosSinComas.splice(1);//Para que comienze a mostrar datos apartir del valor 1 en el arreglo ya que el primero es una marca que no nos sirve
                    

                    Preguntas.push(SinMarca);
                    
                
            }

                //CICLO PARA SACAR PREGUNTAS CON I=0 TRAE LA PREGUNTA Y SUS RESPUESTAS CON I=1 SOLO LAS RESPUESTAS 
                for(var i=1; i < datos.length-1 ;i++){
                    
                    var datosSinComas= datos[i].split(';');
                    var SinMarca = datosSinComas.splice(1);

                    datosPreguntas.push(SinMarca);
                
            }


            //RESPUESTAS DE 1 SOLA FILA (OSEA 1 SOLA PREGUNTA)----Con esto analizar el tamaño de los circulos

            for(var i=1; i < datos.length-1 ;i++){
                    
                var datosSinComas= datos[i].split(';');
                var SinMarca = datosSinComas.splice(1).shift();

                datosPreguntas2.push(SinMarca);
            
        }


                var datos0= datos[0].split(';'); /* obtiene el dato de la posicion cero*/
                var datos1= datos[1].split(';');
                var datos2= datos[2].split(';');
                var datos3= datos[3].split(';');
                var datos4= datos[4].split(';');
                var datos5= datos[5].split(';');
                var datos6= datos[6].split(';');
                var datos7= datos[7].split(';');
                var datos8= datos[8].split(';');
                var datos9= datos[9].split(';');
                //var datos10= datos[10].split(';');

                console.log("Preguntas: "+Preguntas[0]);
                console.log("Respuestas: "+datosPreguntas);
                console.log("Respuestas: "+datosPreguntas2);
      

            /*     console.log(datos0); 
                console.log(datos1);
                console.log(datos2);
                console.log(datos3);
                console.log(datos4);
                console.log(datos5);
                console.log(datos6);
                console.log(datos7);
                console.log(datos8);
                console.log(datos9);
                console.log(datos10); */
    

                                    //Configuracion de la grafica
                        var config = {
                            type: 'radar',
                            data: {
                                //Aqui toca hacer que lea los datos de la primera fila ignorando la primera y ultima posicion ya que por defecto genera un ultimo dato vacio
                                labels: [datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4],datos0[1],datos0[2],datos0[3],datos0[4]],
                                datasets: [{

                                    label: 'Dato',
                                    borderColor: "rgba(0, 0, 255, 0)",
                                    backgroundColor: "rgba(0, 0, 255, 0)",
                                    pointBackgroundColor: "rgba(255, 255,255, 0)", //color de relleno de los puntos
                                    
                                   
                        
                                    radius: 20,
                                    borderWidth:2,
                                    pointBorderColor: "rgba(255, 0, 0, 1)",
                                    pointHoverRadius:20,
                                    pointHoverBackgroundColor: "rgba(255, 0, 0, 0.2)" ,

                                    //AL ENVIAR UN DATA SET QUE TIENE UNA POSICION +1 DE LOS LABEL, ESE NUEVO DATO COMIENZA EN EL PRIMER LABEL (pregunta)
                                    data: [

                                      /*    //tipos de preguntas
                                        1,//P1
                                        2,//P2
                                        3,//P3
                                        1,//P4
                                        3,//P1   Este pasa a ser el 2 dato de la P1 */

                                        datos1[1],datos1[2],datos1[3],datos1[4],//Fila1
                                        datos2[1],datos2[2],datos2[3],datos2[4],//Fila2
                                        datos3[1],datos3[2],datos3[3],datos3[4]//fila3

                                    ]
                                },]
                            },
                            options: {
                                responsive: true,
                                title: {
                                    display: true,
                                    text: 'Anillos de frecuencia'
                                },

                                scale: {
                                    ticks: {
                                        beginAtZero: true,
                                        maxTicksLimit:3
                                        //display:false
                                    }
                                }
                            }

                            
                        };
                            
                                window.myRadar = new Chart(document.getElementById('canvas'), config);
                            
                            var colorNames = Object.keys(window.chartColors);    
                        }   

                }); 